package com.canda.mule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class DatabaseMock {

	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseMock.class);

	public static String loadXml(int aKey, int aNumberOfMockEntries) {
		StringBuilder tempString = new StringBuilder();
		tempString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		tempString.append(System.lineSeparator());
		tempString.append("<product>");
		tempString.append(System.lineSeparator());
		for (int i = 0; i < aNumberOfMockEntries; i++) {
			tempString.append("<attribute" + i + ">");
			tempString.append(System.lineSeparator());
			tempString.append("<key>" + i + "</key>");
			tempString.append(System.lineSeparator());
			tempString.append("<value>ABC</value>");
			tempString.append(System.lineSeparator());
			tempString.append("</attribute" + i + ">");
			tempString.append(System.lineSeparator());
		}
		tempString.append("</product>");
		tempString.append(System.lineSeparator());
		LOGGER.info("Created XML for " + aKey + " length: " + tempString.length());
		return tempString.toString();
	}
}
