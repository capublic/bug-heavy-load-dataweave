package com.canda.mule;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CreateIds {
	public static List<Integer> ids(int aCount) {
		List<Integer> tempList = new ArrayList<>();
		for (int i = 0; i < aCount; i++) {
			tempList.add(i);
		}
		return tempList;
	}
}
